package seguridad;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EFEfrainGutierrez {

	public static void main(String[] args) {
		int i = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Ingresa un entero:");
        try{
            i = Integer.parseInt(br.readLine());
            if(i<1){
            	throw new Exception();
            }
        }catch(NumberFormatException nfe){
            System.err.println("Formato Invalido!");
        } catch (IOException e) {
            System.err.println("Error IO!");
		} catch (java.lang.Exception e) {
			// TODO Auto-generated catch block
            System.err.println("Numero tiene que ser positivo!");
		}
        
        if (isPrime(i)) {
        	System.out.println(i+" SI es primo");
		} else {
        	System.out.println(i+" NO es primo");
        	System.out.println("Un factor es: ");
        	System.out.println(factors(i));
		} 
	}
	
	public static boolean isPrime(int n){
		if (n == 1) {
			return false;
		}
		if (n == 2) {
	    	return true;	
		}
		if (n == 3) {
	    	return true;	
		}
		if (n % 2 == 0) {
	    	return false;	
		}
		if (n % 3 == 0) {
	    	return false;	
		}
		int x = 2;
		while(x < n){
			if(n % x == 0){
				return false;
			}
			x++;
		}
		return true;
    }
	public static String factors(int n){
		for(int i = 2; i < n; i++) {
			if(n % i == 0) {
				System.out.println(i);
				break;
			}
		}
		return "";
	}
}
