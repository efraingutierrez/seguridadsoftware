package seguridad;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CPEfrainGutierrez {

	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		int i = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Ingresa un entero:");
        try{
            i = Integer.parseInt(br.readLine());
            if(i<=2){
            	throw new Exception();
            }
        }catch(NumberFormatException nfe){
            System.err.println("Formato Invalido! (Numero muy largo para ser un int)");
        } catch (IOException e) {
            System.err.println("Error IO!");
		} catch (java.lang.Exception e) {
			// TODO Auto-generated catch block
            System.err.println("Numero tiene que ser positivo y mayor que 2!");
		}
        
        System.out.println(factors(i));
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Tiempo de Ejecucion (ms): "+ totalTime);
	}
	public static boolean isPrime(int n){ // algoritmo del ejercicio pasado
		if (n == 1) {
			return false;
		}
		if (n == 2) {
	    	return true;	
		}
		if (n == 3) {
	    	return true;	
		}
		if (n % 2 == 0) {
	    	return false;	
		}
		if (n % 3 == 0) {
	    	return false;	
		}
		int x = 2;
		while(x < n){
			if(n % x == 0){
				return false;
			}
			x++;
		}
		return true;
    }
	public static String factors(int n){
		StringBuilder sb = new StringBuilder();
		for(int i = 2; i <= n; i++) {
			if(i==2){
				sb.append(i + ", ");
			}else{
				if(isPrime(i)){
					sb.append(i+ ", ");
				}
			}
		}
		return sb.toString();
	}
}
